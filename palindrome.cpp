#include <iostream>
#include <string>
#include <sstream>

using namespace std;

string toString(int i){
	string data;
	stringstream sts; sts << i;
	sts >> data;
	return data;
}

bool isPalindrome(string st){
	for(int f=0, l=st.size()-1; f<st.size(); f++, l--){
		if(st[f]!=st[l])
			return false;
	}

	return true;
}

int main(){
	int x;
	int y;
	int product;
	int maxPal;


	for(x=100; x<1000; x++){
		for(y=100; y<1000; y++){
			
			product= y*x;
			int tmp = product;
			string st = toString(product);
			if(isPalindrome(st) && (tmp > maxPal)){
				maxPal = tmp;
			}
		}
	}

	cout<<endl<<maxPal<<endl;

return 0;
}
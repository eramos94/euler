#include <iostream>
using namespace std;

int sumOfNat(int n){
	return(int((n*(n+1))/2));
}

int sumOfSquares(int n){
	return(int((n*(n+1)*(2*n+1))/6));
}

int main(){

	int x = (sumOfNat(100)*sumOfNat(100)) - sumOfSquares(100);

	cout<<endl<<x<<endl<<endl;
	return 0;

}
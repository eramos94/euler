#include <iostream>

using namespace std;

bool isDivisible(int x){
	for(int i=2; i<21; i++){
		if(x%i!=0){
			return false;
		}
	}

	return true;

}
int main(){

	bool found = false;
	for(int i=2520; !found; i+=20){
		if(isDivisible(i)){
			found = true;
			cout<<endl<<i<<endl<<endl;
		}
	}


	return 0;
}
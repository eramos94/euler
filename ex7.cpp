#include <iostream>
#include <cmath>

bool isPrime(int n){
	double x = sqrt(n);
	for(int i=2; i<=x; i++){
		if(n%i==0){
			return false;
		}
	}
	return true;
}


using namespace std;
int main(){
	int primes = 1; //por el 2

	for(int i=3; primes <= 10001; i++){
		if(isPrime(i)){
			primes++;
		}
		if(primes == 10001){
			cout<< endl<<i<<endl<<endl;
			break;
		}
	}

	return 0;
}